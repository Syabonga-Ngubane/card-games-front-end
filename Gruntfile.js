module.exports = function(grunt) {
  grunt.initConfig({
		connect: {
			server: {
        options: {
          port: 9001,
          basePath: 'games',
          keepalive: true
        }
			}
		},

    includeSource: {
      options: {
        basePath: 'games'
      },
      myTarget: {
        files: {
          'games/card-games.html': 'games/card-games.tpl.html'
        }
      }
    },

    jshint: {
      all: ['js/**/*.js', 'css/*.css']
    },

    watch: {
      js: {
        files: ['js/**/*.js','games/**/*.html'],
        tasks: ['concat']
      },
      options: {
        livereload:true
      }
    },

    express: {
      all: {
        options: {
          port: 9001,
          hostname: 'localhost',
          bases: ['.'],
          livereload: true
        }
      }
    },

    concat: {
      missing: {
        src: ['js/**/*.js'],
        dest: 'build/js/scripts.js',
        nonull: true,
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-include-source');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express');

  grunt.registerTask('default',['express','watch']);
};

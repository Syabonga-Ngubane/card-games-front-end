var canvas = this.__canvas = new fabric.Canvas('canvas', {
    backgroundColor: '#115821',
    hoverCursor: 'pointer',
    selection: true }),

    piles = [],
    frontEndStockPile = [],
    frontEndTopRowWasteHeaps = [[],[],[],[]],
    frontEndBottomRowWasteHeaps = [[],[],[],[]],
    frontEndWasteHeaps = [[],[],[],[]],
    frontEndFoundations = [[],[],[],[]],
    frontEndBlockElevenLayout = [[],[],[],[],
                                [],[],[],[],
                                [],[],[],[],];

fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

canvas.on('object:selected', function(e) {
  if(e.target instanceof Pile) {
    var splitRes = e.target.id.split('-');
    var pileId = splitRes[0];

    switch (pileId) {
      case 's':
        restCall('play', e.target, frontEndStockPile);
        break;
      case 'f':
        restCall('play', e.target, frontEndFoundations[parseInt(splitRes[1])]);
        break;
      case 'w':
        restCall('play', e.target, frontEndWasteHeaps[parseInt(splitRes[1])]);
        break;
      case 'l':
        restCall('play', e.target, frontEndBlockElevenLayout[parseInt(splitRes[1])]);
        break;
    }
  }
});

/* Init */
function littleSpider(backEndStockPile, backEndTopRowWasteHeaps, backEndBottomRowWasteHeaps, backEndFoundations) {
  var layoutCoords = [], coord = {}, card, pile;

  /* Layout coordinates */
  for (var row = 0; row < 3; row++) {
    for (var col = 0; col < 4; col++) {
      coord.left = 495 + col*155;
      coord.top = 115 + row*225;
      layoutCoords.push(coord);
      coord = {};
    }
  }

  /* Little Spider top row waste heaps */
  for (var i = 0; i <= 3; i++) {
    pile = new Pile('tw-'+i, layoutCoords[i].left, layoutCoords[i].top, 0.1, true);
    canvas.add(pile);
  }

  /* Little Spider foundations */
  for (var i = 0; i <= 3; i++) {
    var backEndFoundation = backEndFoundations[i];

    card = new fabric.Group(cardObjects(backEndFoundation[0].rank, backEndFoundation[0].suit), {
      left: layoutCoords[i+4].left,
      top: layoutCoords[i+4].top,
      hasControls: false,
      hasBorders: false,
      evented: false
    });
    canvas.add(card);
    frontEndFoundations[i].push(card);

    pile = new Pile('f-'+i, layoutCoords[i+4].left, layoutCoords[i+4].top, 0.1, true);
    canvas.add(pile);
  }

  /* Little Spider bottom row waste heaps */
  for (var i = 8; i <= layoutCoords.length-1; i++) {
    pile = new Pile('bw-'+(i-8), layoutCoords[i].left, layoutCoords[i].top, 0.1, true);
    canvas.add(pile);
  }

  /* Stock Pile */
  pile = new Pile('s', 80, 340.5, 0.1, true);

  /* Stock pile initialization */
  for (var i = 0; i <= backEndStockPile.length-1; i++) {
    card = new fabric.Group(cardObjects(backEndStockPile[i].rank, backEndStockPile[i].suit), {
      hasControls: false,
      hasBorders: false,
      evented: false,
      left: 80,
      top: 340
    });
    canvas.add(card);
    frontEndStockPile.push(card);
  }

  canvas.add(pile);

  return 'Little Spider';
};

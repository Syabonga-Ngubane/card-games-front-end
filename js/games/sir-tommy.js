/* Init */
function sirTommy(backEndStockPile, backEndWasteHeaps, backEndFoundations) {
  var layoutCoords = [], coord = {}, card, pile;

  /* Layout coordinates */
  for (var row = 0; row < 2; row++) {
    for (var col = 0; col < 4; col++) {
      coord.left = 285 + col*155;
      coord.top = 115 + row*225;
      layoutCoords.push(coord);
      coord = {};
    }
  }

  /* Sir Tommy foundations */
  for (var i = 0; i <= 3; i++) {
    pile = new Pile('f-' + i, layoutCoords[i].left, layoutCoords[i].top, 0.1, true);
    canvas.add(pile);
  }

  /* Sir Tommy waste heaps */
  for (var i = 4; i <= layoutCoords.length-1; i++) {
    pile = new Pile('w-' + (i-4), layoutCoords[i].left, layoutCoords[i].top, 0.1, true);
    canvas.add(pile);
  }

  /* Stock Pile */
  pile = new Pile('s', 520, 565, 0.1, true);

  /* Stock pile initialization */
  for (var i = 0; i <= backEndStockPile.length-1; i++) {
    card = new fabric.Group(cardObjects(backEndStockPile[i].rank, backEndStockPile[i].suit), {
      hasControls: false,
      hasBorders: false,
      evented: false,
      left: pile.left,
      top: pile.top+1
    });
    canvas.add(card);
    frontEndStockPile.push(card);
  }

  canvas.add(pile);

  return 'Sir Tommy';
};

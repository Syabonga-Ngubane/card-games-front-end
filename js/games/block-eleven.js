/* Init */
function blockEleven(backEndStockPile, backEndLayout) {
  var layoutCoords = [], coord = {}, pile, card;

  /* Layout coordinates */
  for (var col = 0; col < 4; col++) {
    for (var row = 0; row < 3; row++) {
      coord.left = 495 + col*155;
      coord.top = 115 + row*225;
      layoutCoords.push(coord);
      coord = {};
    }
  }

  /* Block Eleven layout initialization */
  for (var i = backEndLayout.length-1; i >= 0; i--) {
    var layoutPile = backEndLayout[i];

    card = new fabric.Group(cardObjects(layoutPile[0].rank, layoutPile[0].suit), {
      left: layoutCoords[i].left,
      top: layoutCoords[i].top,
      hasControls: false,
      hasBorders: false,
      evented: false
    });

    canvas.add(card);
    frontEndBlockElevenLayout[i].push(card);

    pile = new Pile('l-' + i, layoutCoords[i].left, layoutCoords[i].top, 0.1, 'black');
    canvas.add(pile);
    piles.push(pile);
  }

  /* Stock pile initialization */
  for (var i = 0; i <= backEndStockPile.length-1; i++) {
    card = new fabric.Group(cardObjects(backEndStockPile[i].rank, backEndStockPile[i].suit), {
      hasControls: false,
      hasBorders: false,
      evented: false,
      left: 80,
      top: 115
    });

    canvas.add(card);
    frontEndStockPile.push(card);
  }

  pile = new Pile('s', 80, 115.5, 1, '#3366cc');
  canvas.add(pile);

  return 'Block Eleven';
};

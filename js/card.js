/* Pile */
function Pile(id, left, top, opacity, fill) {
  this.opacity = opacity;
  this.fill = fill;
  this.left = left;
  this.top = top-1;
  this.id = id;
}

Pile.prototype = new fabric.Rect({
  hasControls: false,
  hasBorders: false,
  stroke: '#333333',
  strokeWidth: 1,
  evented: false,
  height: 221,
  width: 151,
  rx: 9,
  ry: 9
});

Pile.prototype.constructor = Pile;

/* Card Frame */
function CardFrame(id) {
  this.id = id;
}

CardFrame.prototype = new fabric.Rect({
  fill: '#e5e5ff', //'#ffe5e5', //"#dddddd",
  stroke: '#333333',
  strokeWidth: 1,
  height: 220,
  width: 150,
  rx: 8,
  ry: 8
});

CardFrame.prototype.constructor = CardFrame;

/* Card Suit */
function CardSuit(text, fill) {
  this.text = text;
  this.fill = fill;
}

CardSuit.prototype = new fabric.Text('', {
  fontFamily: 'Card-Characters',
  textAlign: 'center',
  fontSize: 140,
  top: 20
});

CardSuit.prototype.constructor = CardSuit;

/* Card Rank */
function CardRank(text, fill, angle, left, top) {
  this.angle = angle;
  this.text = text;
  this.fill = fill;
  this.left = left;
  this.top = top;
}

CardRank.prototype = new fabric.Text('', {
  fontFamily: 'Card-Characters',
  originX: 'center',
  originY: 'center',
  fontSize: 30
});

CardRank.prototype.constructor = CardRank;

/* Methods */

function getSuit(cardSuit) {
  switch (cardSuit) {
    case 'CLUBS':
      return String.fromCharCode(93);
    case 'DIAMONDS':
      return String.fromCharCode(91);
    case 'HEARTS':
      return String.fromCharCode(123);
    case 'SPADES':
      return String.fromCharCode(125);
  }
}

function getCardFill(cardSuit) {
  switch (cardSuit) {
    case 'SPADES':
    case 'CLUBS':
      return '#00004d';
    case 'HEARTS':
    case 'DIAMONDS':
      return 'red';
  }
}

function getRank(cardRank) {
  switch (cardRank.toString()) {
    case '1':
      return 'A';
    case '10':
      return String.fromCharCode(61);
    case '11':
      return 'J';
    case '12':
      return 'Q';
    case '13':
      return 'K';
    default:
      return cardRank.toString();
  }
}

function getId(cardRank, cardSuit) {
  switch (cardRank.toString()) {
    case '1':
      return 'A' + cardSuit.substring(0, 1);
    case '11':
      return 'J' + cardSuit.substring(0, 1);
    case '12':
      return 'Q' + cardSuit.substring(0, 1);
    case '13':
      return 'K' + cardSuit.substring(0, 1);
    default:
      return cardRank + cardSuit.substring(0, 1);
  }
}

function cardObjects(rank, suit) {
  cardFrame = new CardFrame(getId(rank,suit));
  cardSuit = new CardSuit(getSuit(suit), getCardFill(suit));
  topCardRank = new CardRank(getRank(rank), getCardFill(suit), 0, -60, -80);
  bottomCardRank = new CardRank(getRank(rank), getCardFill(suit), 180, 60, 80);
  return [cardFrame, topCardRank, bottomCardRank, cardSuit ];
}

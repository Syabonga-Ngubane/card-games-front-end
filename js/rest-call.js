function restCall(execution, selectedPile, frontEndStackPile) {
  var xhr = new XMLHttpRequest(),
      method = 'GET',
      openPiles,
      url,
      gameName;

  switch (execution) {
    case 'new-game':
      url = 'http://localhost:8080/new-game';
      break;
    case 'play':
      url = 'http://localhost:8080/play?pileId=' + selectedPile.id;
      break;
    default:
      return;
  }

  if ('withCredentials' in xhr) {
    xhr.open(method, url);
  } else {
    alert('Your browser does not support Cross-Origin Resource Sharing (CORS)');
    return;
  }

  xhr.onload = function() {
    var gameStatusResponse = xhr.responseText;
    var gameStatus = JSON.parse(gameStatusResponse);

    var div = document.getElementById('statusMessage'),
        message = gameStatus.statusMessage;

    div.innerHTML = message;
    openPiles = gameStatus.openPilesIndices;

    var backEndFoundations = gameStatus.foundations,
        backEndWasteHeaps = gameStatus.wasteHeaps,
        backEndTopRowWasteHeaps = gameStatus.topRowWasteHeaps,
        backEndBottomRowWasteHeaps = gameStatus.bottomRowWasteHeaps,
        backEndStockPile = gameStatus.stockPile,
        backEndLayout = gameStatus.layout,
        isUpdated = gameStatus.isUpdated,
        move = gameStatus.currentMove,
        openPilesIndices = gameStatus.openPilesIndices;

    switch (execution) {
      case 'new-game':
        blockEleven(backEndStockPile, backEndLayout);
        openPilesUpdate(openPilesIndices);
        //calculation(backEndStockPile, backEndWasteHeaps, backEndFoundations);
        //sirTommy(backEndStockPile, backEndWasteHeaps, backEndFoundations);
        //littleSpider(backEndStockPile, backEndTopRowWasteHeaps, backEndBottomRowWasteHeaps, backEndFoundations);
        break;
      case 'play':
        updateStatus(selectedPile, frontEndStackPile, move, isUpdated, openPilesIndices);
        break;
      default:
        return;
    }
  };

  xhr.send();
  return 'Block Eleven';
}

function openPilesUpdate(openPilesIndices) {
  for (var i = 0; i < piles.length; i++) {
    var splitRes = piles[i].id.split('-');
    var pileIndex = splitRes[1];

    if(openPilesIndices.indexOf(parseInt(pileIndex)) == '-1') {
      piles[i].opacity = 0.5;
    } else {
      piles[i].opacity = 0.2;
    }
    canvas.renderAll();
  }
}

function updateStatus(selectedPile, frontEndStackPile, move, isUpdated, openPilesIndices) {
  var selectedCard;
  switch (move) {
    case 'draw build card':
      if(isUpdated) {
        canvasUpdateObject.buildCardView = canvasUpdateObject.selectedDrawPileStack.pop();
        canvasUpdateObject.selectedBuildPile = selectedPile;
        canvasUpdateObject.selectedBuildPileStack = frontEndStackPile;
        canvasUpdateObject.selectedBuildPileStack.push(canvasUpdateObject.buildCardView);

        canvasUpdateObject.buildCardView.bringToFront();
        animateMove();
        canvasUpdateObject.selectedBuildPile.bringToFront();
        openPilesUpdate(openPilesIndices);
      } else {
        shake(selectedPile);
      }
      break;
    case 'build pile':
      if(isUpdated) {
        canvasUpdateObject.selectedDrawPile = selectedPile;
        canvasUpdateObject.selectedDrawPileStack = frontEndStackPile;
      } else {
        shake(selectedPile);
      }
      break;
  }
}

function topMove(newTopValue, selectedCard, moveDuration) {
  selectedCard.animate('top', newTopValue, {
    duration: moveDuration,
    onChange: canvas.renderAll.bind(canvas),
    onComplete: function() {},
    easing: fabric.util.ease['easeInOutQuint']
  });
}

function animateMove() {
  var moveDuration = 500,
      newLeftValue = canvasUpdateObject.selectedBuildPile.left,
      newTopValue = canvasUpdateObject.selectedBuildPile.top+1,
      selectedCard = canvasUpdateObject.buildCardView;
  try {
    selectedCard.animate('left', newLeftValue, {
      duration: moveDuration,
      onChange: topMove(newTopValue, selectedCard, moveDuration),
      onComplete: function() {},
      easing: fabric.util.ease['easeInOutQuint']
    });
  } catch(err) {
    shake(selectedPile);
  }
}

function shakeBack(selectedPile, fill, opacity) {
  var moveDuration = 5;
  try {
    selectedPile.animate('left', selectedPile.getLeft() - 5, {
      duration: moveDuration,
      onChange: canvas.renderAll.bind(canvas),
      onComplete: function() {
        selectedPile.opacity = opacity;
        selectedPile.fill = fill;
      },
      easing: fabric.util.ease['easeInCubic']
    });
  } catch(err) {}
}

function shake(selectedPile) {
  var moveDuration = 5;
  var fill = selectedPile.fill;
  var opacity = selectedPile.opacity;
  selectedPile.opacity = 0.3;
  selectedPile.fill = 'red';

  try {
    selectedPile.animate('left', selectedPile.getLeft() + 5, {
      duration: moveDuration,
      onChange: canvas.renderAll.bind(canvas),
      onComplete: function() {
        shakeBack(selectedPile, fill, opacity);
      },
      easing: fabric.util.ease['easeInCubic']
    });
  } catch(err) {}
}
